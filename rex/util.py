import asyncio
import configparser
import functools
import sys


def async_to_sync(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(func(*args, **kwargs))
    return wrapper


async def ssh(config: configparser.SectionProxy, command, forward_x=False):
    args = get_ssh_args(config)
    if forward_x:
        args.append("-X")
    else:
        args.append("-x")
    args.append(command)

    # execute command
    print(f"+ ssh {' '.join(args)}")
    return await asyncio.create_subprocess_exec("ssh", *args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)

async def scp_get(config: configparser.SectionProxy, source: str, target: str, recursive=False):
    # get destination
    dest = config.get("host")
    if user := config.get("user"):
        dest = f"{user}@{dest}"

    # compose arguments
    args: list[str] = ["-p"]
    if port := config.get("port"):
        args.append("-P")
        args.append(port)
    if recursive:
        args.append("-r")
    args.append(f"{dest}:{source}")
    args.append(target)

    # execute command
    print(f"+ scp {' '.join(args)}")
    return await asyncio.create_subprocess_exec("scp", *args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)


async def rput(config: configparser.SectionProxy, source: str, target: str):
    # get destination
    dest = config.get("host")
    if user := config.get("user"):
        dest = f"{user}@{dest}"

    # compose command
    args = get_ssh_args(config)
    cmd = f"tar -c --zstd -f - -C {source} . | ssh {' '.join(args)} 'tar -x --zstd -f - -C {target}'"

    # execute command
    print(f"+ {cmd}")
    return await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)


def get_ssh_args(config):
    # get destination
    dest = config.get("host")
    if user := config.get("user"):
        dest = f"{user}@{dest}"

    # compose arguments
    args: list[str] = [dest]
    if port := config.get("port"):
        args.append("-p")
        args.append(port)
    return args

async def scp_put(config: configparser.SectionProxy, source: str, target: str, recursive=False):
    # get destination
    dest = config.get("host")
    if user := config.get("user"):
        dest = f"{user}@{dest}"

    # compose arguments
    args: list[str] = ["-p"]
    if port := config.get("port"):
        args.append("-P")
        args.append(port)
    if recursive:
        args.append("-r")
    args.append(source)
    args.append(f"{dest}:{target}")

    # execute command
    print(f"+ scp {' '.join(args)}")
    return await asyncio.create_subprocess_exec("scp", *args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)


def die(message, *args, **kwargs):
    print(message, *args, file=sys.stderr, **kwargs)
    sys.exit(1)
