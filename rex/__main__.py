import atexit
import click
import configparser
from uuid import uuid4
from pathlib import Path
import os
from .util import async_to_sync, die, rput, scp_put, ssh


REQUIRED_OPTIONS = {"host"}
INSTANCE = uuid4()  # who am I?
SOURCE = os.getcwd()  # where am I?
TARGET = f"~/.rex/{INSTANCE}"  # where do I need to execute?

@click.command()
@click.option("-c", "--config", "path", default=".rex.ini")
@click.option("-s", "--section", "sect", default="DEFAULT")
@click.argument("command", nargs=-1)
@async_to_sync
async def main(path, sect, command: str):
    """Program to executing programs remotely."""
    path = Path(path)
    if not path.exists():
        die(f"rex: file {path} is not present")

    # parse configuration
    config = configparser.ConfigParser()
    try:
        config.read(path)
    except configparser.ParsingError as e:
        die(f"rex: config {path} is invalid: {e.message}")

    # get section
    if sect != "DEFAULT" and not config.has_section(sect):
        die(f"rex: no config {sect} in {path}")
    section: configparser.SectionProxy = config[sect]

    # check variables
    keys = set(section.keys())
    if not keys.issuperset(REQUIRED_OPTIONS):
        missing = sorted(REQUIRED_OPTIONS - keys)
        s = " and ".join((", ".join(missing[:-1]), missing[-1]))
        die(f"rex: options {s} are missing in section {section.name}")

    # register cleanup
    atexit.register(async_to_sync(cleanup), section)

    # execute command remotely
    await do(section, " ".join(command))


async def do(config: configparser.SectionProxy, command: str):
    """Executes the command."""
    # create directory
    process = await ssh(config, f"mkdir -p {TARGET}")
    stdout, stderr = await process.communicate()
    print(stdout.decode(), end="")
    if stderr:
        die(stderr.decode(), end="")

    # provide context
    process = await rput(config, SOURCE, TARGET)
    stdout, stderr = await process.communicate()
    print(stdout.decode(), end="")
    if stderr:
        die(stderr.decode(), end="")

    # execute command
    process = await ssh(config, f"cd {TARGET} && {command}")
    stdout, stderr = await process.communicate()
    print(stdout.decode(), end="")
    if stderr:
        die(stderr.decode(), end="")


async def cleanup(config):
    """Cleans the remote files."""
    process = await ssh(config, f"rm -r ~/.rex/{INSTANCE}")
    stdout, stderr = await process.communicate()
    print(stdout.decode(), end="")
    if stderr:
        die(stderr.decode(), end="")


if __name__ == '__main__':
    main()
